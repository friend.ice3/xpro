# Skill Effects Progression
## Novice
- KO - First Aid
- OK - Trick Dead
## Swordman
- KO - Bash
- OK - Magnum Break
- OK - Endure
- OK - Provoke
## Mage
- OK - Stone Curse
- OK - Cold Bolt
- OK - Lighting Bolt
- KO - Napalm Beat
- OK - Fire Bolt
- OK - Sight
- OK - Frost Diver
- OK - Thunder Storm
- OK - Soul Strike
- OK - Fire Ball
- OK - Safety Wall
- OK - Fire Wall
## Archer
- KO - Double Strafe
- KO - Arrow Shower
- KO - Improve Concentration
## Acolyte
- OK - Ruwach
- KO - Holy Light
- OK - Heal1 (small)
- OK - Heal2 (big)
- OK - Offensive Heal
- OK - Teleport
- OK - Cure
- OK - Increase Agi
- OK - Blessing
- OK - Angelus
- OK - Warp Portal
- OK - Pneuma
- OK - Signum Crusis
- KO - Decrease Agi
## Merchant
- OK - Mammonite
- OK - Vending
- OK - Item Appraisal
- KO - Cart Revolution
- KO - Change Cart 1/2
- OK - Crazy Uproar
## Thief
- KO - Steal
- KO - Evenom
- OK - Hide
- OK - Detoxify
- OK - Back Slide
- KO - Pick Stone
- KO - Throw Stone
- KO - Sprinkle Sand