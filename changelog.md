## Changelog
# 17/08/2020 - v3.17
- Add emoticon UI btn
- entityName option is now available (show all mob/user/npc names around)
- Char deletion allows empty input + sit animation
- Add new exe versions support

# 17/08/2020 - v3.16
- Create shadow sprites for preloading
- Default UI background is now windows before img is loaded

# 17/08/2020 - v3.15
- Add new exe version support
- Fix problem with equipment windows

# 17/08/2020 - v3.14
- Fix issue with NPC menu (scrolling)
- New packet for storage/cart/guild storage supported

# 13/08/2020 - v3.13
- Equipment UI fixed for new packets
- Support for new inventory packet

# 13/07/2020 - v3.12
- Add support for 20131223 and 20161228

# 25/06/2020 - v3.11
- Replace 3D flags by SPR
- Add 20170614 packetver support

# 10/06/2020 - v3.10
- Disable Hat and Disable NPC options available in settings for slow connexions
- Optimization in remote client: option to convert all .bmp into .png/.jpg (for faster loading)

# 04/06/2020 - v3.9
- Video background disabled on iOS
- Support custom extra buttons in mobile UI 

# 25/05/2020 - v3.8
- Fix storage/vending for old packets (previous 2018)
- Add 20160106 exe version support
- Add 20150916 exe version support
- Adding support of new bunch of lua :
    - accname.lua
    - jobname.lua
    - pcjobname.lua
    - shadowtable.lua
    - pcjobnamegender.lua
    - jobidentity.lua
    - accessoryid.lua
- Small sign on 3D context map removed (gonna be improved later)
- Remove selection of text when you hold click on map
- New feature : chatroom creation supported (icon added in top left icons)
- Fix issue of others cooldowns that affect you

# 18/05/2020 - v3.7
- Add error message when init loading failed
- Add 20151104 exe version support

# 10/05/2020 - v3.6
- Fix shuffling packet bug in old packets

# 08/05/2020 - v3.5
- Fix context menu (trade)
- Improve Prompt BOX (autosend/history of messages)
- Fix input number/string NPC
- ChatRoom message fixed
- itemInfo.lua and itemInfo.json are now supported (json is the fastest)

# 06/05/2020 - v3.4
- Support of old packets/UI

# 05/05/2020 - v3.3
- Joystick improvements : smarter selection of mob on autoattack and remove delay when moving with pad
- Custom Button/Login supported
- YouTube video Background in login screen supported

# 30/04/2020 - v3.2
- Minified itemInfo.json support
- Button glitches removed when clicking on it

# 29/04/2020 - v3.1
- Support of old packet versions with old UI
- PatcherClientVersion.txt file moved to configs.php
- Opening vending/chatbox with touch fixed
- Login background moved from .bmp to .jpg for smaller size

# 27/04/2020 - v3.0
- Standardized prompt/notification boxes
- Chatbox reverted to original
- Add a button to quick chat on UI
- Skill tree fixes (small tap for requirements, double tap for upgrade and long tap for dragndrop)
- iOS official release
- Questlog duplication fixed
- Some Packets fixed

# 24/02/2020 - v2.6
- Fix item shortcut when teleporting
- Fix Vending 0 zeny 

# 21/02/2020 - v2.5
- Add XPRO version to intro page
- Fix Vending safety check popup

# 21/02/2020 - v2.4
- Add new user count (smooth, busy, normal)

# 20/02/2020 - v2.3
- New Drag and Drop system on old devices (better feeling)
- Fix max default top position SkillList/Storage/Questlog/Equip/Cashshop/Cart
- NPC Cashshop Supported
- Remove [0] from item name

# 18/02/2020 - v2.2
- Fix Skill tree/list
- Possible in option to disable cashshop

# 16/02/2020 - v2.1
- Fix Joystick direction when rotating camera

# 13/02/2020 - v2.0
- Add authorization token to communicate with remote client
- ItemInfo.lua to .json conversion is now made on server

# 12/02/2020 - v1.5
- Cache system created to load faster resources
- Options UI added on background page to clean cached resources and reset UI preferences

# 11/02/2020 - v1.4
- Creation of XPRO intro when launching app
- Creation of video Background additional feature + fancy glowing progress bar
- Possibility to add server logo on Background page
- Fix UI issues in Cashshop
- Fix Questlog UI position

# 29/01/2020 - v1.3
-  Add XPRO logo on intro page

# 21/01/2020 - v1.2
-  Setup intro page with possibility to import Grf located on your device

# 20/01/2020 - v1.1
-  Add some missing packets

# 15/01/2020 - v1.0
- Fix cart problems
- Fix small issue related to Chatbox when warping
- Official creation of XPRO version system - v1.0 just landed!

# 14/01/2020 - v0.2
- Add security for out of screen NPC windows
- Fix NPC store windows
- Fix Vending skill
- ZoomUI now affects pop-up for NPC store

# 13/01/2020 - v0.1
- Zoom options for Joystick and Big Shortcuts in settings for UI
- 50% and 60% added to UI Zoom
- Bug fix of chatbox not returning to initial position
- Cross click on Basic Info, Chatbox input and Minimap options fixed

# 08/01/2020 - v0.0
- Doram is now optionnal
- Left Moving bar for chatbox (mobile version)
- Zoom options in settings for UI
