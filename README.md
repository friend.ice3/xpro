# Cross-Platform Ragnarok Online Client
## License
**While purchasing XPRO client, you must be aware and accept the following terms :**
 - **1 year licence** for **1 server IP** (renewable at end).
 - When you purchase a licence, you agree that your version will contain **only** available features *(and known bugs that might be described here : [Gitlab XPRO Board Issues](https://gitlab.com/xpro1/xpro/issues) )*.
- You agree that the XPRO client is still in progress and **some features could not work or be broken**. 
- You will be delivered a version "as it". Future updates will be subject to fees.
- The author **is not committed** to correct bugs.
- If you want to propose a new feature **or** report bug, you or your players can post an issue here :  [Gitlab XPRO Board Issues](https://gitlab.com/xpro1/xpro/issues), it's open to everyone who follow the rules.
- The author guarantees minimum **1 major update every 2 months** with some bug fixes (and potentially new features).
- Update costs will depends on your number of players *(will be cheaper for small servers)* and buying update automatically renews licence for 1 year.

## Disclaimer
*Do not rely only on XPRO client.*

*XPRO is made to help out players that cannot download your full client (at work office, school, internet store, etc) but it cannot be the only client used for your server and you should always encourage players to download standard client.*

*In order to not break your Gepard Shield, we don't want to support it in XPRO (but alternative will be available soon for XPRO).*

*XPRO downloads resources when it needs it, so you can update your grf and players that arrive will get the updates (No Patcher needed).*

## Prerequisites
 1. Using an updated version of rAthena or Hercules *(XPRO supports Gepard)*.
 2. Recommended packet version is **2018-06-20** *(XPRO official support)*.
 3. A web server where you can upload your full client (no encrypted GRF here).
 4. A virtual server where you are hosting your emulator to start **wsproxy**.
 5. From 1 to 2 weeks for delivery after purchase.

## Recommended Specs
_**Required to play smoothly:** Min. 2GB RAM and Good connexion (WiFi or 4G+/5G) with unlimited bandwith._
### Android
Chrome Browser v81
### iOS
iOS Safari v13.3


### Tested Devices
- [x] Samsung Galaxy S9+
- [x] iPhone 8+
- [x] iPhone X Pro Max
- [x] iPad Air 2
- [x] Wiko U pulse lite

